package com.proyecto.backtingeso;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackTingesoApplication {

	public static void main(String[] args) {
		SpringApplication.run(BackTingesoApplication.class, args);
	}

}
