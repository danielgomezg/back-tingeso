package com.proyecto.backtingeso.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

import com.proyecto.backtingeso.repositories.AlumnoRepository;
import com.proyecto.backtingeso.models.Alumno;

@RestController
@RequestMapping("/")
@CrossOrigin(origins = "*")
public class AlumnoController{
    
    @Autowired
    AlumnoRepository repository;

    //obtener alumno
    @GetMapping("/alumnos")
    public List<Alumno> getAllNotes() {
        return repository.findAll();
    }

    //insertar alumno
    @PostMapping("/alumno")
    public Alumno storeAlumno(@RequestBody Alumno newAlumno){
        return repository.save(newAlumno);
    }

    //obtener un producto 
    @GetMapping("/alumno/{id}")
    public int updateAlumno(@RequestBody Alumno updateAlumno, @PathVariable Long id){
        repository.findById(id)
            .map(Alumno -> {
                Alumno.setNombre(updateAlumno.getNombre());
                Alumno.setRut(updateAlumno.getRut());
                Alumno.setFechaNacimiento(updateAlumno.getFechaNacimiento());
                Alumno.setCarrera(updateAlumno.getCarrera());
                repository.save(Alumno);
            return 0;
            });
        return 0;
    }

    //Eliminiar 
    @DeleteMapping("/Alumno/{id}")
    public void deleteAlumno(@PathVariable Long id){
        repository.deleteById(id);
    }
}