
package com.proyecto.backtingeso.repositories;

import com.proyecto.backtingeso.models.Alumno;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface AlumnoRepository extends JpaRepository<Alumno, Long>{
    Alumno findAlumnoById(Long id);
}





